/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   header.h                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: rel-bour <rel-bour@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/10/04 17:06:16 by rel-bour          #+#    #+#             */
/*   Updated: 2021/11/26 05:54:42 by rel-bour         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef HEADER_H
#define HEADER_H

#include <stdio.h>
#include <readline/readline.h>
#include <readline/history.h>
#include <stdbool.h>
#include <limits.h>
#include <stdio.h>
#include <stdlib.h>//for exit
#include <unistd.h> // for fork
#include <string.h>
#include <signal.h>
#include <fcntl.h>
#include <sys/wait.h> // for wait 

// HEADER FILE FOR PARSING
#include "parsing/parse_header.h"

typedef struct s_filerdr
{
    char *all;
    char *name;
    int type;
	char *org_name;
	int ambiguous;
    struct s_filerdr *next;
} t_filerdr;

typedef struct s_cmds
{
	int	s_code;
	int	i_fl;
	char	**filenames;
    char *line; // line kaml
    char *command; // sing command kaml bla pip
	char **all;
    char *type; // command name
    char **arguments;
    int option;
    t_filerdr *redrctions;
    struct s_cmds *next_p;
    
} t_cmds;

t_cmds *init_stuct();
int main_parsing(char **envs);



// thisi for linked list
t_cmds		*new_node(char *str);
void		join_list(t_cmds *tmp, t_cmds *src);
void		clear_red_list(t_filerdr *f);
void		init_linkedlist(char **str);
void		free_redrection(t_filerdr *f);
void		clear_list(t_cmds *tmp);
t_filerdr	*new_red_list(char *str);
t_filerdr	*init_header_list_rd(t_filerdr *org_list, int p, char **str);
int			*get_indexs_rd(t_cmds *test, int *indexs_rd, int *i);
t_cmds		*data_if_not_red(t_cmds *test);
t_cmds		*data_if_red_exist(t_cmds *test, int *ret_value);
t_cmds		*cmd_alloc(int i);
t_filerdr	*red_alloc(int i);
void		join_red_list(t_filerdr *add_tmp, t_filerdr *src);




#endif
