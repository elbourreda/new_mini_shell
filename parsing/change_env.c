/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   change_env.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: rel-bour <rel-bour@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/11/08 21:58:28 by rel-bour          #+#    #+#             */
/*   Updated: 2021/11/26 08:13:13 by rel-bour         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../header.h"

char	*ret_change_env_qts(t_env_no_qts evq, int *j, char *str, int j2)
{
	evq.data = getenv(evq.ret);
	if (!evq.data)
		evq.data = "";
	evq.temp = evq.tmp;
	evq.tmp = ft_strjoin(evq.tmp, evq.data);
	free(evq.temp);
	evq.s = ft_substr(str, (*j), strlen((str + (*j))));
	evq.temp = evq.tmp;
	evq.tmp = ft_strjoin(evq.tmp, evq.s);
	free(evq.temp);
	(*j) = (j2 + (strlen(evq.data)));
	free(evq.ret);
	free(evq.s);
	free(str);
	return (evq.tmp);
}

char	*change_env_half_qots(char *str, int *j, int j2)
{
	t_env_no_qts	evq;
	t_cmds	*data;

	data = init_stuct();
	evq.ret = strdup("");
	(*j)--;
	evq.tmp = ft_substr(str, 0, (*j));
	(*j)++;
	if (str[*j] && (ft_isalpha(str[*j]) || str[*j] == '_'))
	{
		while (str[*j] && (ft_isalnum(str[*j]) || str[*j] == '_'))
		{
			evq.temp = evq.ret;
			evq.ret = strjoin1(evq.ret, str[*j]);
			free(evq.temp);
			(*j)++;
		}
	}
	else if (str[*j] && str[*j] == '?')
	{
		(*j)++;
		str = status_env_ret_qts(evq.tmp, str, *j);
		free(evq.ret);
		// (*j) = (j2);
		(*j) = (j2 + (strlen(ft_itoa(data->s_code)) - 1));
		data->s_code = 0;
		return (str);
	}
	return (ret_change_env_qts(evq, j, str, j2));
}

char	*ret_chng_env(t_env_no_qts ev, int *j, char *str, int j2)
{
	ev.data_alloc = 0;
	ev.data = getenv(ev.ret);
	if (!ev.data)
		ev.data = "";
	else
	{
		ev.data = addqouts(ev.data);
		ev.data_alloc = 1;
	}
	ev.temp = ev.tmp;
	ev.tmp = ft_strjoin(ev.tmp, ev.data);
	free(ev.temp);
	ev.s = ft_substr(str, (*j), strlen((str + (*j))));
	ev.temp = ev.tmp;
	ev.tmp = ft_strjoin(ev.tmp, ev.s);
	free(ev.temp);
	(*j) = (j2 + (strlen(ev.data) - 1));
	free(ev.ret);
	if (ev.data_alloc == 1)
		free(ev.data);
	free(ev.s);
	free(str);
	return (ev.tmp);
}

char	*change_env_no_qots(char *str, int *j, int j2)
{
	t_env_no_qts	ev;
	t_cmds	*data;

	data = init_stuct();
	ev.ret = strdup("");
	(*j)--;
	ev.tmp = ft_substr(str, 0, (*j));
	(*j)++;
	if (str[*j] && (ft_isalpha(str[*j]) || str[*j] == '_'))
	{
		while (str[*j] && (ft_isalnum(str[*j]) || str[*j] == '_'))
		{
			ev.temp = ev.ret;
			ev.ret = strjoin1(ev.ret, str[*j]);
			free(ev.temp);
			(*j)++;
		}
	}
	else if (str[*j] && str[*j] == '?')
	{
		(*j)++;
		str = status_env_ret(ev.tmp, str, *j);
		free(ev.ret);
		(*j) = (j2 + (strlen(ft_itoa(data->s_code)) - 1));
		data->s_code = 0;
		return (str);
	}
	return (ret_chng_env(ev, j, str, j2));
}

char	**change_env(char **str)
{
	int	i;
	int	j;
	int	j2;

	i = 0;
	while (str[i])
	{
		j = -1;
		str[i] = change_env_while(str[i], j, j2);
		i++;
	}
	return (str);
}
