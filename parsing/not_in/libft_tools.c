/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   libft_tools.c                                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: rel-bour <rel-bour@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/11/22 11:21:43 by rel-bour          #+#    #+#             */
/*   Updated: 2021/11/26 06:02:31 by rel-bour         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */



#include "../../header.h"


char	*ft_substr(char const *s, unsigned int start, size_t len)
{
	char	*dst;
	size_t	i;

	i = 0;
	if (s == NULL)
		return (NULL);
	if (start > strlen(s))
		len = 0;
	// dst = (char *)mal+loc(len + 1);
	dst = char_alloc(len + 1);
	if (dst == NULL)
		return (NULL);
	while (i < len && s[i] != '\0')
	{
		dst[i] = s[start];
		start++;
		i++;
	}
	dst[i] = '\0';
	return (dst);
}

int		ft_isalpha(int c)
{
	if ((c > 64 && c < 91) || (c > 96 && c <= 122))
		return (1);
	return (0);
}

char	*ft_strcpy(char *dest, const char *src)
{
	int i;

	i = 0;
	while (src[i] != '\0')
	{
		dest[i] = src[i];
		i++;
	}
	dest[i] = '\0';
	return (dest);
}

char	*ft_strjoin(char const *s1, char const *s2)
{
	size_t	size_s1;
	char	*strjoin;

	if (!s1 || !s2)
		return (NULL);
	size_s1 = strlen(s1);
	// strjoin = (char*)mal+loc(sizeof(char) * (strlen(s1) + strlen(s2) + 1));
	strjoin = char_alloc(strlen(s1) + strlen(s2) + 1);
	if (&strjoin[0] == 0)
		return (0);
	ft_strcpy(&strjoin[0], s1);
	ft_strcpy(&strjoin[size_s1], s2);
	return (strjoin);
}

int	ft_isprint(int c)
{
	if (c > 32 && c <= 126)
		return (1);
	else
		return (0);
}

int		ft_isalnum(int c)
{
	if (c >= 48 && c <= 57)
		return (1);
	else if ((c >= 'a' && c <= 'z') || (c >= 'A' && c <= 'Z'))
		return (1);
	else
		return (0);
}

static int	ft_size(int n)
{
	int		size;
	long	nb;

	nb = n;
	size = 1;
	if (nb < 0)
	{
		size++;
		nb = -nb;
	}
	while (nb >= 10)
	{
		size++;
		nb /= 10;
	}
	return (size);
}

char		*ft_itoa(int nb)
{
	char	*str;
	long	nbr;
	int		i;

	i = 0;
	if (!(str = (char*)malloc(sizeof(char) * ft_size(nb) + 1)))
		return (NULL);
	if (nb < 0)
		str[0] = '-';
	nbr = nb;
	if (nbr < 0)
		nbr = -nbr;
	str[ft_size(nb) - i++] = '\0';
	while (nbr >= 10)
	{
		str[ft_size(nb) - i++] = (nbr % 10) + 48;
		nbr /= 10;
	}
	str[ft_size(nb) - i++] = (nbr % 10) + 48;
	return (str);
}