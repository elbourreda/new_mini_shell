# **************************************************************************** #
#                                                                              #
#                                                         :::      ::::::::    #
#    Makefile                                           :+:      :+:    :+:    #
#                                                     +:+ +:+         +:+      #
#    By: rel-bour <rel-bour@student.42.fr>          +#+  +:+       +#+         #
#                                                 +#+#+#+#+#+   +#+            #
#    Created: 2021/10/04 17:04:38 by rel-bour          #+#    #+#              #
#    Updated: 2021/11/23 12:44:09 by rel-bour         ###   ########.fr        #
#                                                                              #
# **************************************************************************** #

NAME = mini_shell

CC = gcc -g   -lreadline -ledit  #-fsanitize=address 

CF = gcc -g   -lreadline -ledit  -fsanitize=address 

CFLAGS = -Wall -Wextra -Werror   

SRC = main.c\
parsing/*.c\
parsing/not_in/*.c

all : $(NAME) 

fs:
	@$(CF) $(SRC) -o $(NAME)
$(NAME): $(SRC)
	@$(CC) $(SRC) -o $(NAME)
clean:
	@rm -rf *.o

fclean: clean
	@rm -rf $(NAME)
	
re:	fclean all

ref:	fclean fs
